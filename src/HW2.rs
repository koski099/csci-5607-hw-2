extern crate debug;
extern crate bmp;
extern crate getopts;

use std::io::{BufferedReader, File};
use std::default::Default;
use bmp::{BMPimage, BMPpixel};
use getopts::{optopt, optflag, getopts, OptGroup};
use std::os;
use std::num::pow;
use std::rc::Rc;
use std::fmt;
use std::f32::consts::PI;


struct Camera {
  // 3D position
  px: f32,
  py: f32,
  pz: f32,
  
  // 3D viewing direction
  dx: f32,
  dy: f32,
  dz: f32,
  
  // 3D Up vector
  ux: f32,
  uy: f32,
  uz: f32,
  
  // One half of the height angle of
  // the viewing frustrum
  ha: int,
}

impl Default for Camera {
  fn default () -> Camera {
    Camera {
      px: 0.0,
      py: 0.0,
      pz: 0.0,
      dx: 0.0,
      dy: 0.0,
      dz: 1.0,
      ux: 0.0,
      uy: 1.0,
      uz: 0.0,
      ha: 45,
    }
  }
}

impl Camera {
  fn get_ray(&self, x: uint, y: uint, width: uint, height: uint, rmat: Vec<f32>) -> Line {
    let ratio = height as f32 / width as f32;
    let hh = height as f32 / 2.0;
    let hw = width as f32 / 2.0;
    
    let distance = (self.ha as f32 * PI / 180.0).tan();
    
    let p2 = Point {
      x: self.px,
      y: self.py,
      z: self.pz,
    };
    
    let p1 = self.dot(&rmat, Point {
      x:  0.0,
      y: -(0.5 + x as f32 - hw) / (hw * ratio),
      z: (0.5 + y as f32 - hh) / hh,
    }) + p2 + self.direction().times(distance);
    
    let vec = (p1 - p2).unit();
    
    Line {
      x: p1.x,
      y: p1.y,
      z: p1.z,
      dx: vec.x,
      dy: vec.y,
      dz: vec.z,
      .. Default::default()
    }
  }
  
  fn direction(&self) -> Point {
    (Point {
      x: self.dx,
      y: self.dy,
      z: self.dz,
    }).unit()
  }
  
  fn up(&self) -> Point {
    (Point {
      x: self.ux,
      y: self.uy,
      z: self.uz,
    }).unit()
  }
  
  fn get_rotation_matrix(&self) -> Vec<f32> {
    let M = Point {
      x: 1.0,
      y: 0.0,
      z: 0.0,
    };
    
    let N = self.direction();
    
    let c = (M * N) / (M.norm() * N.norm());
    let s = (1.0 - c * c).sqrt();
    let C = 1.0 - c;
    
    vec![
      N.x*N.x*C+c,     N.x*N.y*C-N.z*s, N.x*N.z*C+N.y*s,
      N.y*N.x*C+N.z+s, N.y*N.y*C+c,     N.y*N.z*C+N.x*s,
      N.z*N.x*C-N.y*s, N.z*N.y*C-N.x*s, N.z*N.z*C+c,
    ]
  }
  
  fn dot(&self, rmat: &Vec<f32>, p: Point) -> Point {
    Point {
      x: rmat[0] * p.x + rmat[1] * p.y + rmat[2] * p.z,
      y: rmat[3] * p.x + rmat[4] * p.y + rmat[5] * p.z,
      z: rmat[6] * p.x + rmat[7] * p.y + rmat[8] * p.z,
    }
  }
}

struct Sphere {
  // 3D position
  x: f32,
  y: f32,
  z: f32,
  
  // Radius
  r: f32,
  
  // Associated material
  material: Rc<Material>,
}

impl Default for Sphere {
  fn default () -> Sphere {
    Sphere {
      x: 0.0,
      y: 0.0,
      z: 0.0,
      r: 0.0,
      material: Rc::new(Material { ..Default::default() }),
    }
  }
}

impl Sphere {
  fn intersects(&self, line: Line) -> Option<Point> {
    let origin = line.origin();
    let distance = origin - self.position();
    let loc = line.direction() * distance;
    let under_radical = pow(loc, 2) - distance.mag_squared() + pow(self.r, 2);
    
    // No intersection
    if under_radical < 0.0 {
      return None
    }
    
    // Intersects, but backwards from the origin
    if loc > 0.0 {
      return None
    }
    
    let point = origin + line.direction().times(-loc - under_radical.sqrt());
    Some(point)
  }
  
  fn color(&self, point: Point, ambient_light: &AmbientLight, 
            point_lights: &Vec<PointLight>, pixel: Point) -> Color {
    
    let diff_color = self.get_diffuse_color();
    let spec_color = self.get_specular_color();
    let mut lit_color = self.get_ambient_color() * *ambient_light;
    
    for light in point_lights.iter() {
      if !self.in_light(point, light) {
        continue;
      }
      
      let L = (light.position() - point).unit();
      let N = (point - self.position()).unit();
      let d = point - light.position();
      let dot = (L * N) * (4.0 / d.magnitude());
      
      let R = L - N.times(2.0 * (L * N));
      let V = (point - pixel).unit();
      let dot2 = pow((R * V), self.material.ns as uint) * (1.0 / d.magnitude());
      
      let color = light.color();
      
      let diffuse_color = Color {
        r: diff_color.r * dot * color.r,
        g: diff_color.g * dot * color.g,
        b: diff_color.b * dot * color.b,
      };
      
      let specular_color = Color {
        r: spec_color.r * dot2 * color.r,
        g: spec_color.g * dot2 * color.g,
        b: spec_color.b * dot2 * color.b,
      };
      
      lit_color = lit_color + diffuse_color.clamp() + specular_color.clamp();
    }
    
    lit_color.clamp()
  }
  
  fn get_ambient_color(&self) -> Color {
    Color{
      r: self.material.ar,
      g: self.material.ag,
      b: self.material.ab,
    }
  }
  
  fn get_diffuse_color(&self) -> Color {
    Color{
      r: self.material.dr,
      g: self.material.dg,
      b: self.material.db,
    }
  }
  
  fn get_specular_color(&self) -> Color {
    Color{
      r: self.material.sr,
      g: self.material.sg,
      b: self.material.sb,
    }
  }
  
  fn position(&self) -> Point {
    Point {
      x: self.x,
      y: self.y,
      z: self.z,
    }
  }
  
  fn in_light(&self, point: Point, light: &PointLight) -> bool {
    let direction = (light.position() - point).unit();
    
    let line = Line {
      x: point.x,
      y: point.y,
      z: point.z,
      dx: direction.x,
      dy: direction.y,
      dz: direction.z,
    };
    
    match self.intersects(line) {
      Some(p) => (p - point).magnitude() > 0.00005,
      None    => true,
    }
  }
}

struct Color {
  // RGB color
  r: f32,
  g: f32,
  b: f32,
}

impl Color {
  fn clamp(&self) -> Color {
    Color {
      r: clamp(self.r),
      g: clamp(self.g),
      b: clamp(self.b),
    }
  }
}

impl Mul<AmbientLight, Color> for Color {
  fn mul(&self, rhs: &AmbientLight) -> Color {
    Color {
      r: self.r * rhs.r,
      g: self.g * rhs.g,
      b: self.b * rhs.b,
    }
  }
}

impl Add<Color, Color> for Color {
  fn add(&self, rhs: &Color) -> Color {
    Color {
      r: self.r + rhs.r,
      g: self.g + rhs.g,
      b: self.b + rhs.b,
    }
  }
}

struct Point {
  // 3D position
  x: f32,
  y: f32,
  z: f32,
}

impl Point {
  fn norm(&self) -> f32 {
    (pow(self.x, 2) + pow(self.y, 2) + pow(self.z, 2)).sqrt()
  }
  
  fn unit(&self) -> Point {
    let norm = self.norm();
    
    Point {
      x: self.x / norm,
      y: self.y / norm,
      z: self.z / norm,
    }
  }
  
  fn magnitude(&self) -> f32 {
    (pow(self.x, 2) +
     pow(self.y, 2) +
     pow(self.z, 2)).sqrt()
  }
  
  fn mag_squared(&self) -> f32 {
    pow(self.x, 2) +
    pow(self.y, 2) +
    pow(self.z, 2)
  }
  
  fn times(&self, rhs: f32) -> Point {
    Point {
      x: self.x * rhs,
      y: self.y * rhs,
      z: self.z * rhs,
    }
  }
  
  fn cross(&self, rhs: Point) -> Point {
    Point {
      x:   self.y * rhs.z - rhs.y * self.z,
      y: -(self.x * rhs.z - rhs.x * self.z),
      z:   self.x * rhs.y - rhs.x * self.y,
    }
  }
}

impl Add<Point, Point> for Point {
  fn add(&self, rhs: &Point) -> Point {
    Point {
      x: self.x + rhs.x,
      y: self.y + rhs.y,
      z: self.z + rhs.z,
    }
  }
}

impl Sub<Point, Point> for Point {
  fn sub(&self, rhs: &Point) -> Point {
    Point {
      x: self.x - rhs.x,
      y: self.y - rhs.y,
      z: self.z - rhs.z,
    }
  }
}

impl Mul<Point, f32> for Point {
  fn mul(&self, rhs: &Point) -> f32 {
    self.x * rhs.x +
    self.y * rhs.y +
    self.z * rhs.z
  }
}


impl fmt::Show for Point {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Point: ({} {} {})", self.x, self.y, self.z)
  }
}

struct Line {
  // 3D position
  x: f32,
  y: f32,
  z: f32,
  
  // 3D direction
  dx: f32,
  dy: f32,
  dz: f32,
}

impl Line {
  fn origin(&self) -> Point {
    Point {
      x: self.x,
      y: self.y,
      z: self.z,
    }
  }
  
  fn direction(&self) -> Point {
    (Point {
      x: self.dx,
      y: self.dy,
      z: self.dz,
    }).unit()
  }
}

impl fmt::Show for Line {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "Line: ({} {} {}) -> ({} {} {})", self.x, self.y, self.z, self.dx, self.dy, self.dz)
  }
}

impl Default for Line {
  fn default() -> Line {
    Line {
      x: 0.0,
      y: 0.0,
      z: 0.0,
      dx: 0.0,
      dy: 0.0,
      dz: 0.0,
    }
  }
}

impl Mul<f32, Point> for Line {
  fn mul(&self, rhs: &f32) -> Point {
    Point {
      x: self.x * *rhs,
      y: self.y * *rhs,
      z: self.z * *rhs,
    }
  }
}

struct AmbientLight {
  // RGB light
  r: f32,
  g: f32,
  b: f32,
}

impl Default for AmbientLight {
  fn default() -> AmbientLight {
    AmbientLight {
      r: 0.0,
      g: 0.0,
      b: 0.0,
    }
  }
}

struct PointLight {
  // RGB light
  r: f32,
  g: f32,
  b: f32,
  
  // 3D position
  x: f32,
  y: f32,
  z: f32,
}

impl PointLight {
  fn position(&self) -> Point {
    Point {
      x: self.x,
      y: self.y,
      z: self.z,
    }
  }
  
  fn color(&self) -> Color {
    Color {
      r: self.r,
      g: self.g,
      b: self.b,
    }
  }
}

struct Background {
  // RGB light
  r: f32,
  g: f32,
  b: f32,
}

impl Default for Background {
  fn default () -> Background {
    Background {
      r: 0.0,
      g: 0.0,
      b: 0.0,
    }
  }
}

struct Material {
  // Ambient RGB
  ar: f32,
  ag: f32,
  ab: f32,
  
  // Diffuse RGB
  dr: f32,
  dg: f32,
  db: f32,
  
  // Specular RGB
  sr: f32,
  sg: f32,
  sb: f32,
  
  // Phong cosine power for specular highlights
  ns: f32,
  
  // Transmissive RGB
  tr: f32,
  tg: f32,
  tb: f32,
  
  // Index of Refraction
  ior: f32,
}

impl Default for Material {
  fn default () -> Material {
    Material {
      ar:  0.0,
      ag:  0.0,
      ab:  0.0,
      dr:  1.0,
      dg:  1.0,
      db:  1.0,
      sr:  0.0,
      sg:  0.0,
      sb:  0.0,
      ns:  5.0,
      tr:  0.0,
      tg:  0.0,
      tb:  0.0,
      ior: 1.0,
    }
  }
}

fn float(num: &str) -> f32 {
  from_str(num).unwrap()
}

fn integer(num: &str) -> int {
  from_str(num).unwrap()
}

fn clamp(num: f32) -> f32 {
  match num > 1.0 {
    true => return 1.0,
    false => {},
  };
  
  match num < 0.0 {
    true => 0.0,
    false => num,
  }
}

fn main() {
  let args = os::args();
  
  let opts = [
    optopt("s", "scene", "set scene filename", "NAME"),
  ];
  
  let matches = match getopts(args.tail(), opts) {
    Ok(m)  => { m },
    Err(f) => { fail!(f.to_string()) },
  };
  
  if !matches.opt_present("s") {
    fail!("Need a scene file to read from!");
  }

  let (mut width, mut height) = (640u, 480u);
  
  // Singletons
  let mut bg = Background { ..Default::default() };
  let mut ambient_light = AmbientLight { ..Default::default() };
  let mut camera = Camera { ..Default::default() };
  
  // Lists of objects
  let mut materials: Vec<Rc<Material>> = Vec::new();
  materials.push(Rc::new(Material { ..Default::default() }));
  let mut spheres: Vec<Sphere> = Vec::new();
  let mut point_lights: Vec<PointLight> = Vec::new();
  
  let mut output = "raytraced.bmp";
  let path = Path::new(matches.opt_str("s").unwrap());
  let mut file = BufferedReader::new(File::open(&path));
  
  let lines: Vec<String> = file
    .lines()
    .filter(|l| l.clone().unwrap().as_bytes()[0] != '#' as u8)
    .map(|l| l.unwrap())
    .collect();
  
  for line in lines.iter() {
    let attrs: Vec<&str> = line.as_slice().trim().split(' ').collect();
    
    match attrs[0] {
      "material" => {
        materials.push(Rc::new(Material {
          ar:  float(attrs[1]),
          ag:  float(attrs[2]),
          ab:  float(attrs[3]),
          dr:  float(attrs[4]),
          dg:  float(attrs[5]),
          db:  float(attrs[6]),
          sr:  float(attrs[7]),
          sg:  float(attrs[8]),
          sb:  float(attrs[9]),
          ns:  float(attrs[10]),
          tr:  float(attrs[11]),
          tg:  float(attrs[12]),
          tb:  float(attrs[13]),
          ior: float(attrs[14]),
        }));
      },
      "camera" => {
        camera = Camera {
          px: float(attrs[1]),
          py: float(attrs[2]),
          pz: float(attrs[3]),
          dx: float(attrs[4]),
          dy: float(attrs[5]),
          dz: float(attrs[6]),
          ux: float(attrs[7]),
          uy: float(attrs[8]),
          uz: float(attrs[9]),
          ha: integer(attrs[10]),
        };
      },
      "sphere" => {
        spheres.push(Sphere {
          x: float(attrs[1]),
          y: float(attrs[2]),
          z: float(attrs[3]),
          r: float(attrs[4]),
          material: (*materials.last().unwrap()).clone(),
        });
      },
      "ambient_light" => {
        ambient_light = AmbientLight {
          r: float(attrs[1]),
          g: float(attrs[2]),
          b: float(attrs[3]),
        };
      },
      "point_light" => {
        point_lights.push(PointLight {
          r: float(attrs[1]),
          g: float(attrs[2]),
          b: float(attrs[3]),
          x: float(attrs[4]),
          y: float(attrs[5]),
          z: float(attrs[6]),
        });
      },
      "background" => {
        bg = Background {
          r: float(attrs[1]),
          g: float(attrs[2]),
          b: float(attrs[3]),
        };
      },
      "film_resolution" => {
        width  = integer(attrs[1]) as uint;
        height = integer(attrs[2]) as uint;
      },
      "output_image" => {
        output = attrs[1].clone();
      },
      ""         => {},
      _          => {
        //println!("Unknown attribute '{}'!", attrs[0]);
      }
    }
  }
  
  let mut img = BMPimage::new(width as i32, height as i32);
  
  let rmat = camera.get_rotation_matrix();
  
  for y in range(0u, height) {
    for x in range(0u, width) {
      let line = camera.get_ray(x, y, width, height, rmat.clone());
      
      // Default to background color
      img.set_pixel(x, y, BMPpixel{
        r: (255.0 * bg.r) as u8,
        g: (255.0 * bg.g) as u8,
        b: (255.0 * bg.b) as u8});
      
      for sphere in spheres.iter() {
        match sphere.intersects(line) {
          Some(point) => {
            let color = sphere.color(point, &ambient_light, &point_lights, line.origin());
            
            //println!("point: {} {} {}", point.x, point.y, point.z);
            
            img.set_pixel(x, y, BMPpixel{
              r: (255.0 * color.r) as u8,
              g: (255.0 * color.g) as u8,
              b: (255.0 * color.b) as u8})
          },
          None => {},
        }
      }
    }
  }
  
  img.save(output);
}
